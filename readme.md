# Pausing Development (for now)
Honestly I don't really have much to say, it's mostly just been a lack of motivation. Especially since Atom sunsetted, which I was using for the compilation of files. I'll need to figure out a way of easily compiling them again. I might come back to this in the future or sooner. But if I were to continue this, I would rather it be contributing to Gitea itself, than having it be a big pile of messy CSS code added on top of what's already there.

# Gitea Modern
Changes the layout of Gitea to give it a more modern look. Can be used with any other themes that change only colors!
| ![Gitea Modern Repository Page](images/codeberg-modern_repoPg.png) | ![Gitea Modern Explore Page](images/codeberg-modern_explorePg.png) | ![Gitea Modern Profile Page](images/codeberg-modern_profilePg.png)
|---|---|---|

**Note:** The [Stylus browser extension](https://add0n.com/stylus.html) and [Stylus preprocessor](https://stylus-lang.com/) are two different things.

### Usage as a Gitea theme, [or use Stylus to apply to any instance](https://userstyles.world/style/529)
To install the theme for all Gitea users:

0. Make sure you're on the latest version of Gitea!
1. Download the [`theme-gitea-modern.css` file][theme-file] and add it to your custom folder in `$GITEA_CUSTOM/public/css/` ([learn how to find that directory][doc-dir]).
  - The path has changed in Gitea 1.15, there the file needs to be placed in `$GITEA_CUSTOM/public/assets/css`
  - Gitea 1.16 uses the old path at `$GITEA_CUSTOM/public/css/`
2. Adapt your `app.ini` to make it the default theme or be user-selectable:
    - To change the default theme, change the value of `DEFAULT_THEME` in the [ui section][doc-config] of `app.ini` to `gitea-modern`

    - To make a theme selectable by users in their profile settings, append `,gitea-modern` to the list of `THEMES` in your `app.ini`.
3. Restart Gitea
4. Enjoy :)

### Contributing
To make changes, always base them on [`theme-gitea-modern.styl`](Gitea/theme-gitea-modern.styl), which requires the [Stylus preprocessor](https://stylus-lang.com/).
When making contributions, don't use the compiled version for your changes.

[theme-file]: Gitea/theme-gitea-modern.css
[usertheme-file]: Gitea/theme-gitea-modern.user.css
[doc-config]: https://docs.gitea.io/en-us/config-cheat-sheet/#ui-ui
[doc-dir]: https://docs.gitea.io/en-us/faq/#where-does-gitea-store-what-file
